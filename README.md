# CU Theme Base

The CU Base theme is based on the [Bootstrap Barrio](https://www.drupal.org/project/bootstrap_barrio) theme, which is following the "so far" defined Drupal 10 theming practices. This means that it is a snapshot of the theme at the time of creation. Currently it is set to the 5.5.6 release, which will need to be manually updated with new releases.

This theme is a backbone type theme, which does not need to be installed but is automatically installed for a sub-theme. The CU Base theme is a UIS site theme that has basic CU branding elements. To theme your site you will either need to select one of our predefined themes or follow the sub-theming instructions below. The theme also utilizes the CU Bundles modules: Content, Media, Layout Paragraphs, Style-guide.

CU Base settings without a sub-theme should have configuration settings set for Base UIS theme.